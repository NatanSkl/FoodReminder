package com.gregskl.foodreminder;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Gregory on 11-Sep-16.
 */
public class FoodAdapter extends RecyclerView.Adapter<FoodAdapter.ViewHolder> {

    private List<Food> foods;
    private AppCompatActivity app;

    public FoodAdapter(List<Food> foods, AppCompatActivity app) {
        this.foods = foods;
        this.app = app;
    }

    @Override
    public FoodAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list, null);
        return new ViewHolder(itemLayoutView);
    }

    @Override
    public void onBindViewHolder(FoodAdapter.ViewHolder holder, int position) {
        holder.text.setText(foods.get(position).getText());
        holder.checkbox.setChecked(foods.get(position).isAvailable());
        holder.checkbox.setTag(foods.get(position));
        holder.checkbox.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                CheckBox checkbox = (CheckBox) v;
                Food food = (Food) checkbox.getTag();
                food.setAvailable(checkbox.isChecked());
                ((MainActivity) app).save();
            }
        });
        holder.layout.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                RelativeLayout layout = (RelativeLayout) v;
                final TextView textview = (TextView) layout.getChildAt(0);
                new AlertDialog.Builder(app)
                        .setTitle("Delete a Food:")
                        .setMessage("Are you sure you want to delete \"" + textview.getText() + "\"?")
                        .setNegativeButton(android.R.string.cancel, null)
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            @Override public void onClick(DialogInterface dialog, int which) {
                                List<Food> foods = ((MainActivity) app).foods;
                                Food toRemove = null;
                                for (Food food : foods) {
                                    if(food.getText().equals(textview.getText()))
                                        toRemove = food;
                                }
                                foods.remove(toRemove);
                                ((MainActivity) app).adapter.notifyDataSetChanged();
                                ((MainActivity) app).save();
                            }
                        })
                        .create()
                        .show();
                return false;
            }
        });
    }

    @Override
    public int getItemCount() {
        return foods.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView text;
        public CheckBox checkbox;
        public RelativeLayout layout;

        public ViewHolder(View itemView) {
            super(itemView);
            text = (TextView) itemView.findViewById(R.id.text);
            checkbox = (CheckBox) itemView.findViewById(R.id.checkbox);
            layout = (RelativeLayout) itemView.findViewById(R.id.layout);
        }
    }
}
